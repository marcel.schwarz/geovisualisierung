let dataset = {
    2012: [128.52, 187.54, 63.16, 72.18, 0, 0, 0, 0, 6, 68.58, 84.54, 120.51], //Month from Jan to Dec
    2013: [144.44, 133.07, 121.78, 38.27, 35.53, 18.57, 0, 0, 16.56, 48.53, 91.88, 115.2], //Month from Jan to Dec
    2014: [113.77, 96.17, 77.74, 38.27, 29.79, 11.6, 0, 0, 16.85, 36.14, 67.55, 138] //Month from Jan to Dec
}

const monthNames = {
    short: ["Jan", "Feb", "Mrz", "Apr", "Mai", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dez"],
    long: ["Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"]
}

const lookupMonth = (num, short = false) => short ? monthNames.short[num] : monthNames.long[num];

function drawPieChartYear() {
    let pieChartData = google.visualization.arrayToDataTable([
        ["Jahr", "MWh"],
        ["2012", dataset["2012"].reduce((acc, curr) => acc + curr)],
        ["2013", dataset["2013"].reduce((acc, curr) => acc + curr)],
        ["2014", dataset["2014"].reduce((acc, curr) => acc + curr)]
    ])

    let chart = new google.visualization.PieChart(document.getElementById("piechartYear"))
    chart.draw(pieChartData, {
        title: "Wärmebedarf 2012 - 2014 (in MWh)",
        colors: ["#0040ff", "#ff0000", "#155a00"],
        height: 400
    })
}

function drawPieChartMonth() {
    let parsedData = []
    for (let i = 0; i < 12; i++) {
        parsedData.push([lookupMonth(i), dataset["2012"][i] + dataset["2013"][i] + dataset["2014"][i]])
    }

    let pieChartData = google.visualization.arrayToDataTable([
        ["Monat", "MWh"],
        ...parsedData
    ])

    let chart = new google.visualization.PieChart(document.getElementById("piechartMonth"))
    chart.draw(pieChartData, {
        title: "Akkumulierter Wärmebedarf 2012 - 2014 pro Monat (in MWh)",
        height: 400
    })
}

function drawLineChart() {
    let parsedData = []

    for (let i = 0; i < 12; i++) {
        parsedData.push([lookupMonth(i), dataset["2012"][i], dataset["2013"][i], dataset["2014"][i]])
    }

    let lineChartData = google.visualization.arrayToDataTable([
        ["Monat", "2012", "2013", "2014"],
        ...parsedData
    ])

    let chart = new google.visualization.LineChart(document.getElementById("linechart"))
    chart.draw(lineChartData, {
        title: "Wärmebedarf 2012 - 2014 (in MWh)",
        colors: ["#0040ff", "#ff0000", "#155a00"],
        explorer: {},
        height: 400
    })
}

function drawScatterChart() {
    let parsedData = []

    for (let i = 0; i < 12; i++) {
        parsedData.push([lookupMonth(i), dataset["2012"][i], dataset["2013"][i], dataset["2014"][i]])
    }

    let data = new google.visualization.arrayToDataTable([
        ["Monat", "2012", "2013", "2014"],
        ...parsedData
    ])

    let options = {
        chart: {
            title: "Wärmebedarf 2012 - 2014 (in MWh)"
        },
        colors: ["#0040ff", "#ff0000", "#155a00"],
        vAxis: {title: "MWh"},
        hAxix: {title: "Month"},
        explorer: {},
        height: 400
    }
    let chart = new google.charts.Scatter(document.getElementById("scatterchart"))
    chart.draw(data, google.charts.Scatter.convertOptions(options))
}

function drawBubbleChart() {
    let parsedData = []

    for (let year in dataset) {
        let complete = dataset[year].reduce((acc, curr) => acc + curr)
        dataset[year].forEach((value, idx) => {
            parsedData.push([
                lookupMonth(idx, true), idx + 1, (value / complete) * 100, year, value
            ])
        })
    }

    let data = google.visualization.arrayToDataTable([
        //BubbleName, X-Axis, Y-Axis, Series/Color, BubbleSize
        ["Monat", "Monat", "Prozent des Jahresverbrauchs", "Jahr", "MWh"],
        ...parsedData
    ]);

    let options = {
        title: "Wärmebedarf 2012 - 2014 (in MWh)",
        height: 400,
        hAxis: {
            title: "Monat",
            ticks: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
        },
        vAxis: {
            title: "Prozentualer MWh Anteil"
        },
        colors: ["#0040ff", "#ff0000", "#155a00"],
        explorer: {},
        bubble: {textStyle: {fontSize: 11}},
    };

    let chart = new google.visualization.BubbleChart(document.getElementById("bubblechart"));
    chart.draw(data, options);
}

google.charts.load("current", {packages: ["corechart", "scatter"]})
google.charts.setOnLoadCallback(drawPieChartYear)
google.charts.setOnLoadCallback(drawPieChartMonth)
google.charts.setOnLoadCallback(drawLineChart)
google.charts.setOnLoadCallback(drawScatterChart)
google.charts.setOnLoadCallback(drawBubbleChart)
window.addEventListener('resize', _.debounce(() => {
    drawPieChartYear()
    drawPieChartMonth()
    drawLineChart()
    drawScatterChart()
    drawBubbleChart()
}))


// Tree Map
function loadTreeMap() {

    const colors = {doc: "#0035b3", png: "#bc0033", pdf: "#009486", dat: "#676767"}

    function pickColor(coordinates, index) {
        let folder = index[0]
        let file = index[1]
        let type = files[folder][file].type
        return {"fill": colors[type]}
    }

    let files = [
        [
            {folder: "ordner1", name: "Datei 1", size: 90, type: "doc"},
            {folder: "ordner1", name: "Datei 2", size: 60, type: "png"},
            {folder: "ordner1", name: "Datei 3", size: 30, type: "pdf"},
        ],
        [
            {folder: "ordner2", name: "Datei 4", size: 40, type: "dat"},
            {folder: "ordner2", name: "Datei 5", size: 20, type: "dat"}
        ]
    ]

    let data = [
        files[0].map(value => value.size), //Folder 1
        files[1].map(value => value.size)  //Folder 2
    ]
    let labels = [
        files[0].map(value => value.name), //Folder 1
        files[1].map(value => value.name)  //Folder 2
    ]
    Treemap.draw("foldermap", 600, 400, data, labels, {"box": pickColor})
}

document.addEventListener('DOMContentLoaded', loadTreeMap, false)
