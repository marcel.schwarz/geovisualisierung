import {Component, EventEmitter, Injectable, OnInit, Output} from '@angular/core';
import {IDashboardCommonBikePoint} from '../../service/domain/dashboard-common-bike-point';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {IMapBikePoint} from '../../service/domain/map-bike-point';
import {ActivatedRoute} from '@angular/router';
import {DashboardService} from '../../service/dashboard.service';
import {
  ApexAxisChartSeries,
  ApexChart,
  ApexDataLabels,
  ApexFill,
  ApexLegend,
  ApexNoData,
  ApexPlotOptions,
  ApexStroke,
  ApexTitleSubtitle,
  ApexTooltip,
  ApexXAxis,
  ApexYAxis
} from 'ng-apexcharts';
import {DateAdapter, MAT_DATE_FORMATS, NativeDateAdapter} from '@angular/material/core';
import {formatDate} from '@angular/common';

export type ChartOptions = {
  title: ApexTitleSubtitle;
  subtitle: ApexTitleSubtitle;
  series: ApexAxisChartSeries;
  chart: ApexChart;
  colors: string[];
  dataLabels: ApexDataLabels;
  plotOptions: ApexPlotOptions;
  yaxis: ApexYAxis | ApexYAxis[];
  xaxis: ApexXAxis;
  fill: ApexFill;
  tooltip: ApexTooltip;
  stroke: ApexStroke;
  legend: ApexLegend;
  noData: ApexNoData;
};

export const PICK_FORMATS = {
  parse: {dateInput: {month: 'short', year: 'numeric', day: 'numeric'}},
  display: {
    dateInput: 'input',
    monthYearLabel: {year: 'numeric', month: 'numeric'},
    dateA11yLabel: {year: 'numeric', month: 'numeric', day: 'numeric'},
    monthYearA11yLabel: {year: 'numeric', month: 'long'}
  }
};

@Injectable()
class PickDateAdapter extends NativeDateAdapter {
  format(date: Date, displayFormat: Object): string {
    if (displayFormat === 'input') {
      return formatDate(date, 'dd.MM.yyyy', this.locale);
    } else {
      return date.toDateString();
    }
  }
}

export interface StartEndDate {
  actualStartDate: Date;
  actualEndDate: Date;
}

@Component({
  selector: 'app-user-input',
  templateUrl: './user-input.component.html',
  styleUrls: ['./user-input.component.scss'],
  providers: [
    {provide: DateAdapter, useClass: PickDateAdapter},
    {provide: MAT_DATE_FORMATS, useValue: PICK_FORMATS}
  ]
})
export class UserInputComponent implements OnInit {

  @Output() startEndDate: EventEmitter<StartEndDate> = new EventEmitter<StartEndDate>();

  chartOptions: Partial<ChartOptions>;

  station: IDashboardCommonBikePoint;
  maxStartDate: Date;
  maxEndDate: Date;
  form: FormGroup;

  bikePoint: IMapBikePoint;

  constructor(
    private route: ActivatedRoute,
    private service: DashboardService,
    private fb: FormBuilder
  ) {
    this.chartOptions = {
      series: [],
      chart: {
        type: 'bar'
      },
      noData: {
        text: 'Loading...'
      }
    };
  }

  ngOnInit(): void {
    this.form = this.fb.group({
      dateRange: new FormGroup({
        start: new FormControl(),
        end: new FormControl()
      })
    });
    this.route.params.subscribe(params => {
      this.service.fetchDashboardInit(params.id).then(data => {
        this.station = data;
        this.maxStartDate = new Date(data.maxStartDate);
        this.maxEndDate = new Date(data.maxEndDate);
        this.initInput().catch(error => console.log(error));
      });
      this.service.fetchBikePointForStatus(params.id).then(data => {
        this.bikePoint = data;
        const NbBlockedDocks = data.status.NbDocks - data.status.NbBikes - data.status.NbEmptyDocks;
        this.chartOptions = {
          subtitle: {
            text: 'This chart visualizes the availability of the bikes',
            offsetX: 20,
            offsetY: 15,
            style: {
              fontSize: '15px'
            }
          },
          series: [
            {
              name: 'Bikes',
              data: [data.status.NbBikes]
            },
            {
              name: 'Empty docks',
              data: [data.status.NbEmptyDocks]
            },
            {
              name: 'Blocked docks',
              data: [NbBlockedDocks]
            }
          ],
          colors: ['#51ca49', '#8f8e8e', '#f00'],
          chart: {
            type: 'bar',
            height: 180,
            stacked: true,
            toolbar: {
              show: false
            }
          },
          plotOptions: {
            bar: {
              horizontal: true,
              dataLabels: {
                position: 'center'
              }
            }
          },
          dataLabels: {
            enabled: true,
            style: {
              fontSize: '20px',
              colors: ['#fff']
            }
          },
          stroke: {
            show: false
          },
          xaxis: {
            labels: {
              show: false
            },
            axisBorder: {
              show: false
            },
            axisTicks: {
              show: false
            }
          },
          yaxis: {
            show: false,
            title: {
              text: undefined
            },
            axisBorder: {
              show: false
            },
            min: 0,
            max: data.status.NbDocks
          },
          tooltip: {
            enabled: false,
          },
          fill: {
            opacity: 1
          },
          legend: {
            position: 'bottom',
            horizontalAlign: 'right',
            fontSize: '14px'
          }
        };
      });
    });
  }

  async initInput(): Promise<void> {
    const initDate = this.maxEndDate.toISOString().substring(0, 10);
    this.form.get('dateRange').get('start').setValue(initDate);
    this.form.get('dateRange').get('end').setValue(initDate);
  }

  async onSubmit(): Promise<any> {
    this.startEndDate.emit({
      actualStartDate: this.form.get('dateRange').value.start,
      actualEndDate: this.form.get('dateRange').value.end
    });
  }

}
