import {Component, OnInit} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {IDashboardCommonBikePoint} from '../../service/domain/dashboard-common-bike-point';
import {SelectionModel} from '@angular/cdk/collections';
import {MatCheckboxChange} from '@angular/material/checkbox';
import stht from 'seconds-to-human-time';
import {MapService} from '../../service/map.service';
import {DashboardService} from '../../service/dashboard.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {
  displayedColumnsTo: string[] = ['select', 'endStationName', 'number', 'avgDuration', 'marker'];
  displayedColumnsFrom: string[] = ['select', 'startStationName', 'number', 'avgDuration', 'marker'];

  stationToSource = new MatTableDataSource<IDashboardCommonBikePoint>();
  iterableToSource: any[];
  stationFromSource = new MatTableDataSource<IDashboardCommonBikePoint>();
  iterableFromSource: any[];
  selectionModel = new SelectionModel<IDashboardCommonBikePoint>(true, []);
  colors = ['black', 'gray', 'green', 'orange', 'purple', 'red'];
  bikePoint: IDashboardCommonBikePoint;
  maxStartDate: Date;
  maxEndDate: Date;
  isLoadingToSource = true;
  isLoadingFromSource = true;

  constructor(
    private route: ActivatedRoute,
    private map: MapService,
    private service: DashboardService
  ) {
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.colors = ['black', 'gray', 'green', 'orange', 'purple', 'red'];
      this.service.fetchDashboardInit(params.id).then(data => {
        this.bikePoint = data;
        this.maxStartDate = new Date(data.maxStartDate);
        this.maxEndDate = new Date(data.maxEndDate);
        this.initTable();
      });
    });
  }

  initTable(): void {
    this.selectionModel.clear();
    this.map.removeOverlayOnMiniMap();
    const initDate = this.maxEndDate.toISOString().substring(0, 10);
    this.loadData(initDate, initDate);
  }

  onSubmit(actualStartDate: string, actualEndDate: string): void {
    this.resetTableSourcesToDisplaySpinner();
    this.selectionModel.clear();
    this.map.removeOverlayOnMiniMap();
    this.loadData(actualStartDate, actualEndDate);
  }

  resetTableSourcesToDisplaySpinner(): void {
    this.isLoadingToSource = true;
    this.isLoadingFromSource = true;
    this.stationToSource = null;
    this.stationFromSource = null;
    this.iterableToSource = [];
    this.iterableFromSource = [];
  }

  async loadData(actualStartDate: string, actualEndDate: string): Promise<void> {
    this.isLoadingToSource = true;
    this.isLoadingFromSource = true;

    const [stationTo, stationFrom] = await Promise.all([
      this.service.fetchDashboardStationTo(this.bikePoint.id, actualStartDate, actualEndDate),
      this.service.fetchDashboardStationFrom(this.bikePoint.id, actualStartDate, actualEndDate)
    ]);

    this.isLoadingToSource = false;
    this.isLoadingFromSource = false;

    this.colors = ['black', 'gray', 'green', 'orange', 'purple', 'red'];
    this.stationToSource = this.setBikePointColorToSource(stationTo);
    this.iterableToSource = stationTo;
    this.iterableToSource.forEach(bikePoint => bikePoint.polyLineColor = 'red');

    this.stationFromSource = this.setBikePointColorFromSource(stationFrom);
    this.iterableFromSource = stationFrom;
    this.iterableFromSource.forEach(bikePoint => bikePoint.polyLineColor = 'green');

    this.selectionModel.select(...this.iterableFromSource.filter(bikePoint => bikePoint.stationId === this.bikePoint.id));
    this.selectionModel.select(...this.iterableToSource.filter(bikePoint => bikePoint.stationId === this.bikePoint.id));
  }

  public drawIconInTable(bikePoint: any): string {
    return `../../assets/bike-point-${bikePoint.color}.png`;
  }

  humanizeAvgDuration(avgDuration: number): string {
    return stht(avgDuration);
  }

  selectRow(selection: MatCheckboxChange, row): void {
    let markerToDisplay = [];
    this.iterableToSource.forEach(point => {
      if (point.stationId === row.stationId) {
        this.selectionModel.toggle(point);
      }
    });
    this.iterableFromSource.forEach(point => {
      if (point.stationId === row.stationId) {
        this.selectionModel.toggle(point);
      }
    });
    this.selectionModel.selected.forEach(point => {
      markerToDisplay.push(point);
    });
    markerToDisplay = this.changePolyLineColorForDuplicateBikePoints(markerToDisplay);
    this.map.drawTableStationMarker(markerToDisplay);
  }

  changePolyLineColorForDuplicateBikePoints(array: any[]): any[] {
    const id = array.map(item => item.stationId);
    const duplicates = id.filter((value, index) => id.indexOf(value) !== index);
    duplicates.forEach(stationId => {
      array.forEach(bikePoint => {
        if (bikePoint.stationId === stationId) {
          bikePoint.polyLineColor = 'blue';
        }
      });
    });
    return array;
  }

  setBikePointColorToSource(source): any {
    for (const station of source) {
      if (station.stationId === this.bikePoint.id) {
        station.color = 'blue';
        continue;
      }
      station.color = this.getRandomColor();
    }
    return source;
  }

  setBikePointColorFromSource(source): any {
    for (const station of source) {
      if (station.stationId === this.bikePoint.id) {
        station.color = 'blue';
        continue;
      }
      for (const to of this.iterableToSource) {
        if (station.stationId === to.stationId) {
          station.color = to.color;
          break;
        }
      }
      if (!station.color) {
        station.color = this.getRandomColor();
      }
    }
    return source;
  }

  getRandomColor(): string {
    const color = this.colors[Math.floor(Math.random() * this.colors.length)];
    this.colors = this.colors.filter(c => c !== color);
    return color;
  }

  isCheckBoxDisable(row): boolean {
    return row.stationId === this.bikePoint.id;
  }

}
