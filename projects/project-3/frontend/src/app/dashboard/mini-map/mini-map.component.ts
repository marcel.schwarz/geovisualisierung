import { Component, OnInit } from '@angular/core';
import {IDashboardCommonBikePoint} from '../../service/domain/dashboard-common-bike-point';
import {ActivatedRoute} from '@angular/router';
import {DashboardService} from '../../service/dashboard.service';
import {MapService} from '../../service/map.service';

@Component({
  selector: 'app-mini-map',
  templateUrl: './mini-map.component.html',
  styleUrls: ['./mini-map.component.scss']
})
export class MiniMapComponent implements OnInit {

  bikePoint: IDashboardCommonBikePoint;

  constructor(
    private route: ActivatedRoute,
    private service: DashboardService,
    private map: MapService
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.service.fetchDashboardInit(params.id).then(data => {
        this.bikePoint = data;
        this.initMap();
      });
    });
  }

  initMap(): void {
    this.map.initDashboardMap(this.bikePoint.lat, this.bikePoint.lon, 17);
    this.map.drawDashboardStationMarker(this.bikePoint);
  }

}
