import {ChangeDetectionStrategy, Component, OnInit, ViewChild} from '@angular/core';
import {IDashboardCommonBikePoint} from '../service/domain/dashboard-common-bike-point';


import {
  ApexAxisChartSeries,
  ApexChart,
  ApexDataLabels,
  ApexFill,
  ApexLegend,
  ApexNoData,
  ApexPlotOptions,
  ApexStroke,
  ApexTitleSubtitle,
  ApexTooltip,
  ApexXAxis,
  ApexYAxis
} from 'ng-apexcharts';
import {TableComponent} from './table/table.component';
import {RentDurationChartComponent} from './rent-duration-chart/rent-duration-chart.component';
import {RentTimeChartComponent} from './rent-time-chart/rent-time-chart.component';
import {StartEndDate} from './user-input/user-input.component';

export type ChartOptions = {
  title: ApexTitleSubtitle;
  subtitle: ApexTitleSubtitle;
  series: ApexAxisChartSeries;
  chart: ApexChart;
  colors: string[];
  dataLabels: ApexDataLabels;
  plotOptions: ApexPlotOptions;
  yaxis: ApexYAxis | ApexYAxis[];
  xaxis: ApexXAxis;
  fill: ApexFill;
  tooltip: ApexTooltip;
  stroke: ApexStroke;
  legend: ApexLegend;
  noData: ApexNoData;
};

const chartHeight = 460;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  changeDetection: ChangeDetectionStrategy.Default,
})
export class DashboardComponent implements OnInit {

  @ViewChild(TableComponent) table: TableComponent;
  @ViewChild(RentDurationChartComponent) durationChart: RentDurationChartComponent;
  @ViewChild(RentTimeChartComponent) timeChart: RentTimeChartComponent;

  constructor() {
  }

  ngOnInit(): void {
  }

  async onSubmit(startEndDate: StartEndDate): Promise<any> {
    await this.table.onSubmit(
      startEndDate.actualStartDate.toISOString().substring(0, 10),
      startEndDate.actualEndDate.toISOString().substring(0, 10)
    );
    await this.durationChart.onSubmit(
      startEndDate.actualStartDate.toISOString().substring(0, 10),
      startEndDate.actualEndDate.toISOString().substring(0, 10)
    );
    await this.timeChart.onSubmit(
      startEndDate.actualStartDate.toISOString().substring(0, 10),
      startEndDate.actualEndDate.toISOString().substring(0, 10)
    );
  }


}
