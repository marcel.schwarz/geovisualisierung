import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RentDurationChartComponent } from './rent-duration-chart.component';

describe('RentDurationChartComponent', () => {
  let component: RentDurationChartComponent;
  let fixture: ComponentFixture<RentDurationChartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RentDurationChartComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RentDurationChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
