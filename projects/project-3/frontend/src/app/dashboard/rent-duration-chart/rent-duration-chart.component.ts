import {Component, OnInit, ViewChild} from '@angular/core';
import {
  ApexAxisChartSeries,
  ApexChart,
  ApexDataLabels,
  ApexFill,
  ApexLegend,
  ApexNoData,
  ApexPlotOptions,
  ApexStroke,
  ApexTitleSubtitle,
  ApexTooltip,
  ApexXAxis,
  ApexYAxis,
  ChartComponent
} from 'ng-apexcharts';
import {ActivatedRoute} from '@angular/router';
import {DashboardService} from '../../service/dashboard.service';
import {IDashboardCommonBikePoint} from '../../service/domain/dashboard-common-bike-point';

export type ChartOptions = {
  title: ApexTitleSubtitle;
  subtitle: ApexTitleSubtitle;
  series: ApexAxisChartSeries;
  chart: ApexChart;
  colors: string[];
  dataLabels: ApexDataLabels;
  plotOptions: ApexPlotOptions;
  yaxis: ApexYAxis;
  xaxis: ApexXAxis;
  fill: ApexFill;
  tooltip: ApexTooltip;
  stroke: ApexStroke;
  legend: ApexLegend;
  noData: ApexNoData;
};

const chartType = 'duration';

@Component({
  selector: 'app-rent-duration-chart',
  templateUrl: './rent-duration-chart.component.html',
  styleUrls: ['./rent-duration-chart.component.scss']
})
export class RentDurationChartComponent implements OnInit {

  @ViewChild(ChartComponent) chart: ChartComponent;
  chartOptions: Partial<ChartOptions>;

  bikePoint: IDashboardCommonBikePoint;
  maxStartDate: Date;
  maxEndDate: Date;
  isLoading: boolean;

  constructor(
    private route: ActivatedRoute,
    private service: DashboardService,
  ) {
    this.chartOptions = {
      series: [],
      chart: {
        type: 'bar'
      },
      noData: {
        text: 'Loading...'
      }
    };
  }

  ngOnInit(): void {
    this.isLoading = true;
    this.route.params.subscribe(params => {
      this.service.fetchDashboardInit(params.id).then(data => {
        this.bikePoint = data;
        this.maxStartDate = new Date(data.maxStartDate);
        this.maxEndDate = new Date(data.maxEndDate);
        this.initChart().catch(error => console.log(error));
      });
    });
  }

  async initChart(): Promise<void> {
    const initDate = this.maxEndDate.toISOString().substring(0, 10);
    await this.service.fetchDashboardStationCharts(this.bikePoint.id, initDate, initDate, chartType).then(source => {
      this.isLoading = false;
      this.chartOptions = {
        series: [
          {
            name: 'amount of drives',
            data: source.map(value => value.number)
          }
        ],
        chart: {
          type: 'bar',
          height: '460',
          toolbar: {
            show: false
          }
        },
        colors: ['#017bfe'],
        plotOptions: {
          bar: {
            horizontal: false,
            columnWidth: '55%',
            endingShape: 'flat'
          }
        },
        dataLabels: {
          enabled: false
        },
        stroke: {
          show: true,
          width: 2,
          colors: ['transparent']
        },
        xaxis: {
          title: {
            text: 'average rental duration'
          },
          categories: source.map(value => value.minutesGroup),
          labels: {
            formatter: value => {
              return value + ' min';
            }
          }
        },
        yaxis: {
          title: {
            text: 'amount of drives'
          }
        },
        noData: {
          text: 'loading'
        },
        fill: {
          opacity: 1
        }
      };
    });
  }

  async onSubmit(actualStartDate: string, actualEndDate: string): Promise<void> {
    this.isLoading = true;
    this.service.fetchDashboardStationCharts(
      this.bikePoint.id,
      actualStartDate,
      actualEndDate,
      chartType
    ).then(source => {
      this.isLoading = false;
      setTimeout(() => {
        this.chart.updateSeries([{
          data: source.map(value => value.number)
        }]);
      }, 1000);
    });
  }

}
