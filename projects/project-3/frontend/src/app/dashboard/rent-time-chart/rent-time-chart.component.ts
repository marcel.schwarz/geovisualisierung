import {Component, OnInit, ViewChild} from '@angular/core';
import {
  ApexAxisChartSeries,
  ApexChart,
  ApexDataLabels,
  ApexFill,
  ApexLegend,
  ApexNoData,
  ApexPlotOptions,
  ApexStroke,
  ApexTitleSubtitle,
  ApexTooltip,
  ApexXAxis,
  ApexYAxis,
  ChartComponent
} from 'ng-apexcharts';
import {DashboardService} from '../../service/dashboard.service';
import {ActivatedRoute} from '@angular/router';
import {IDashboardCommonBikePoint} from '../../service/domain/dashboard-common-bike-point';

export type ChartOptions = {
  title: ApexTitleSubtitle;
  subtitle: ApexTitleSubtitle;
  series: ApexAxisChartSeries;
  chart: ApexChart;
  colors: string[];
  dataLabels: ApexDataLabels;
  plotOptions: ApexPlotOptions;
  yaxis: ApexYAxis | ApexYAxis[];
  xaxis: ApexXAxis;
  fill: ApexFill;
  tooltip: ApexTooltip;
  stroke: ApexStroke;
  legend: ApexLegend;
  noData: ApexNoData;
};

const chartType = 'time';

@Component({
  selector: 'app-rent-time-chart',
  templateUrl: './rent-time-chart.component.html',
  styleUrls: ['./rent-time-chart.component.scss']
})
export class RentTimeChartComponent implements OnInit {

  @ViewChild(ChartComponent) chart: ChartComponent;
  chartOptions: Partial<ChartOptions>;

  bikePoint: IDashboardCommonBikePoint;
  maxStartDate: Date;
  maxEndDate: Date;
  isLoading: boolean;

  constructor(
    private route: ActivatedRoute,
    private service: DashboardService
  ) {
    this.chartOptions = {
      series: [],
      chart: {
        type: 'line'
      },
      noData: {
        text: 'Loading...'
      }
    };
  }

  ngOnInit(): void {
    this.isLoading = true;
    this.route.params.subscribe(params => {
      this.service.fetchDashboardInit(params.id).then(data => {
        this.bikePoint = data;
        this.maxStartDate = new Date(data.maxStartDate);
        this.maxEndDate = new Date(data.maxEndDate);
        this.initChart().catch(error => console.log(error));
      });
    });
  }

  async initChart(): Promise<void> {
    const initDate = this.maxEndDate.toISOString().substring(0, 10);
    await this.service.fetchDashboardStationCharts(this.bikePoint.id, initDate, initDate, chartType).then(source => {
      this.isLoading = false;
      this.chartOptions = {
        series: [
          {
            name: 'amount of drives',
            type: 'bar',
            data: source.map(value => value.number)
          },
          {
            name: 'average rental duration',
            type: 'line',
            data: source.map(value => Math.round(value.avgDuration / 60))
          }
        ],
        tooltip: {
          enabled: true,
          shared: true,
          x: {
            show: true
          }
        },
        chart: {
          toolbar: {
            show: false
          },
          type: 'line',
          height: '495',
          zoom: {
            enabled: true,
          }
        },
        colors: ['#017bfe', '#51ca49'],
        dataLabels: {
          enabled: false,
        },
        stroke: {
          curve: 'straight'
        },
        legend: {
          show: true,
        },
        xaxis: {
          title: {
            text: 'time of the day'
          },
          categories: source.map(value => value.timeFrame),
          tickAmount: 24,
          tickPlacement: 'between'
        },
        yaxis: [{
          title: {
            text: 'amount of drives',
          },
        }, {
          opposite: true,
          title: {
            text: 'average rental duration'
          },
          labels: {
            formatter: (val: number): string => {
              return val + ' min';
            }
          }
        }],
        fill: {
          opacity: 1
        }
      };
    });
  }

  async onSubmit(actualStartDate: string, actualEndDate: string): Promise<void> {
    this.isLoading = true;
    this.service.fetchDashboardStationCharts(
      this.bikePoint.id,
      actualStartDate,
      actualEndDate,
      chartType
    ).then(source => {
      this.isLoading = false;
      setTimeout(() => {
        this.chart.updateSeries([{
          data: source.map(value => value.number)
        }, {
          data: source.map(value => Math.round(value.avgDuration / 60))
        }]);
      }, 1000);
    });
  }

}
