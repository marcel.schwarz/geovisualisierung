import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RentTimeChartComponent } from './rent-time-chart.component';

describe('RentTimeChartComponent', () => {
  let component: RentTimeChartComponent;
  let fixture: ComponentFixture<RentTimeChartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RentTimeChartComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RentTimeChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
