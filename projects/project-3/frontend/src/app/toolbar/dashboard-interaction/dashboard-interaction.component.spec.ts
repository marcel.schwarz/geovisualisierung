import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardInteractionComponent } from './dashboard-interaction.component';

describe('DashboardInteractionComponent', () => {
  let component: DashboardInteractionComponent;
  let fixture: ComponentFixture<DashboardInteractionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DashboardInteractionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardInteractionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
