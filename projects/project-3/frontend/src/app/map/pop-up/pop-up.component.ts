import {Component, OnInit} from '@angular/core';
import {IMapBikePoint} from '../../service/domain/map-bike-point';
import {Router} from '@angular/router';
import {
  ApexAxisChartSeries,
  ApexChart,
  ApexDataLabels,
  ApexFill,
  ApexLegend,
  ApexPlotOptions,
  ApexStroke,
  ApexTitleSubtitle,
  ApexTooltip,
  ApexXAxis,
  ApexYAxis
} from 'ng-apexcharts';

export type ChartOptions = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  colors: string[];
  dataLabels: ApexDataLabels;
  plotOptions: ApexPlotOptions;
  xaxis: ApexXAxis;
  yaxis: ApexYAxis;
  stroke: ApexStroke;
  title: ApexTitleSubtitle;
  tooltip: ApexTooltip;
  fill: ApexFill;
  legend: ApexLegend;
};


@Component({
  selector: 'app-pop-up',
  templateUrl: './pop-up.component.html',
  styleUrls: ['./pop-up.component.scss']
})
export class PopUpComponent implements OnInit {
  station: IMapBikePoint;
  public chartOptions: Partial<ChartOptions>;

  constructor(private router: Router) {
  }

  ngOnInit(): void {
    const NbBlockedDocks = this.station.status.NbDocks - this.station.status.NbBikes - this.station.status.NbEmptyDocks;
    this.chartOptions = {
      series: [
        {
          name: 'Bikes',
          data: [this.station.status.NbBikes]
        },
        {
          name: 'Empty docks',
          data: [this.station.status.NbEmptyDocks]
        },
        {
          name: 'Blocked docks',
          data: [NbBlockedDocks]
        }
      ],
      colors: ['#51ca49', '#8f8e8e', '#f00'],
      chart: {
        type: 'bar',
        height: 125,
        stacked: true,
        toolbar: {
          show: false
        }
      },
      plotOptions: {
        bar: {
          horizontal: true
        }
      },
      stroke: {
        show: false
      },
      xaxis: {
        labels: {
          show: false
        },
        axisBorder: {
          show: false
        },
        axisTicks: {
          show: false
        }
      },
      yaxis: {
        show: false,
        title: {
          text: undefined
        },
        axisBorder: {
          show: false
        },
        min: 0,
        max: this.station.status.NbDocks
      },
      tooltip: {
        enabled: false,
      },
      fill: {
        opacity: 1
      },
      legend: {
        position: 'bottom',
        horizontalAlign: 'right'
      }
    };
  }

  public route(): void {
    this.router.navigate(['/dashboard', this.station.id]);
  }
}
