import {Component, OnInit} from '@angular/core';
import {MapService} from '../service/map.service';


@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {
  constructor(private service: MapService) {

  }

  ngOnInit(): void {
    this.initMapView();
  }

  async initMapView(): Promise<any> {
    this.service.initMap(51.509865, -0.118092, 14);
    await this.service.drawStationMarkers();
    this.service.drawHeatmap();
    this.service.drawAccidents();
  }

}
