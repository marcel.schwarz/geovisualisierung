import {Component, OnDestroy, OnInit} from '@angular/core';
import {MapService} from '../../service/map.service';
import * as internal from 'events';

@Component({
  selector: 'app-auto-refresh',
  templateUrl: './auto-refresh.component.html',
  styleUrls: ['./auto-refresh.component.scss']
})
export class AutoRefreshComponent implements OnInit, OnDestroy {
  isFlagActive: boolean;
  interval: internal;

  constructor(private map: MapService) {
    const storageFlag = JSON.parse(sessionStorage.getItem('auto-refresh'));
    if (storageFlag) {
      this.isFlagActive = storageFlag;
    } else {
      this.isFlagActive = false;
    }
  }

  ngOnInit(): void {
    this.interval = setInterval(() => {
      if (this.isFlagActive) {
        this.map.autoRefresh().catch(error => console.log(error));
      }
    }, 10000);
  }

  ngOnDestroy(): void {
    clearInterval(this.interval);
  }

  onChange(flag: boolean): void {
    sessionStorage.setItem('auto-refresh', JSON.stringify(flag));
  }

}
