export interface IDashboardCommonBikePoint {
  id?: string;
  color?: string;
  commonName?: string;
  lat?: number;
  lon?: number;
  maxEndDate?: string;
  maxStartDate?: string;
}

export class DashboardCommonBikePoint implements IDashboardCommonBikePoint {
  constructor(
    public id?: string,
    public color?: string,
    public commonName?: string,
    public lat?: number,
    public lon?: number,
    public maxEndDate?: string,
    public maxStartDate?: string
  ) {
  }
}
