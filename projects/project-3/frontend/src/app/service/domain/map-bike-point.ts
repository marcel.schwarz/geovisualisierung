export interface IMapBikePoint {
  id?: string;
  commonName?: string;
  lat?: number;
  lon?: number;
  status?: BikePointStatus;
}

export class MapBikePoint implements IMapBikePoint {
  constructor(
    public id?: string,
    public commonName?: string,
    public lat?: number,
    public lon?: number,
    public status?: BikePointStatus
  ) {
  }
}

export class BikePointStatus {
  NbBikes?: number;
  NbEmptyDocks?: number;
  NbDocks?: number;
}
