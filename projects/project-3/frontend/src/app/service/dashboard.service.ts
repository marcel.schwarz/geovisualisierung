import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(private client: HttpClient) {
  }

  public fetchDashboardInit(id: string): Promise<any> {
    return this.client.get(environment.apiUrl + `latest/dashboard/${id}/`).toPromise();
  }

  public fetchBikePointForStatus(id: string): Promise<any> {
    return this.client.get(environment.apiUrl + `latest/bikepoints/${id}/`).toPromise();
  }

  public fetchDashboardStationTo(id: string, startDate: string, endDate: string): Promise<any> {
    return this.client.get(
      environment.apiUrl + `latest/dashboard/${id}/to?start_date=${startDate}&end_date=${endDate}`
    ).toPromise();
  }

  public fetchDashboardStationFrom(id: string, startDate: string, endDate: string): Promise<any> {
    return this.client.get(
      environment.apiUrl + `latest/dashboard/${id}/from?start_date=${startDate}&end_date=${endDate}`
    ).toPromise();
  }

  public fetchDashboardStationCharts(id: string, startDate: string, endDate: string, type: string): Promise<any> {
    return this.client.get(
      environment.apiUrl + `latest/dashboard/${id}/${type}?start_date=${startDate}&end_date=${endDate}`
    ).toPromise();
  }
}
