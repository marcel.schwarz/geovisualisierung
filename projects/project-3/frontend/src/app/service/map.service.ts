import {Injectable} from '@angular/core';
import * as L from 'leaflet';
import 'leaflet.markercluster';
import 'leaflet.heat/dist/leaflet-heat';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {PopUpService} from './pop-up.service';
import {IMapBikePoint} from './domain/map-bike-point';
import {IDashboardCommonBikePoint} from './domain/dashboard-common-bike-point';


const createIcon = color => L.icon({
  iconUrl: `../../assets/bike-point-${color}.png`,
  iconSize: [45, 45],
  iconAnchor: [23, 36],
  popupAnchor: [0, -29]
});

@Injectable({
  providedIn: 'root'
})
export class MapService {

  public map;
  public miniMap;
  bikePoints: Array<IMapBikePoint> = [];
  mapOverlays: any = {};
  layerToDisplay: any = {};
  miniMapMarker: L.layerGroup;
  markerLayer = [];
  polylineLayer = [];
  dashBoardMarker = L.marker;
  dashBoardBikePoint: IDashboardCommonBikePoint;
  layerControl = L.control(null);
  legend = L.control({position: 'bottomleft'});
  accidentLegend = L.control({position: 'bottomleft'});

  constructor(
    private client: HttpClient,
    private popUpService: PopUpService
  ) {
  }

  public async autoRefresh(): Promise<any> {
    this.layerToDisplay = {};
    for (const name in this.mapOverlays) {
      if (this.map.hasLayer(this.mapOverlays[name])) {
        if (this.mapOverlays.Heatmap === this.mapOverlays[name]) {
          this.layerToDisplay.Heatmap = this.mapOverlays[name];
        } else if (this.mapOverlays.Accidents === this.mapOverlays[name]) {
          this.layerToDisplay.Accidents = this.mapOverlays[name];
        }
      }
      this.map.removeLayer(this.mapOverlays[name]);
    }
    await this.drawStationMarkers();
    this.drawHeatmap();
    this.drawAccidents();
  }

  public initMap(lat: number, lon: number, zoom: number): void {
    this.map = L.map('map').setView([lat, lon], zoom);
    this.map.addLayer(new L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: 'Map data <a href="https://openstreetmap.org">OpenStreetMap</a> contributors',
      minZoom: 0,
      maxZoom: 19,
      preferCanvas: true
    }));

    this.accidentLegend.onAdd = () => {
      const getCircle = (color) => {
        return `
        <svg height="16" width="16">
          <circle cx="8" cy="8" r="8" stroke="black" stroke-width="1" fill=${color} />
        </svg>`;
      };

      const div = L.DomUtil.create('div', 'legend legend-accidents');
      div.innerHTML = `
        <h4>Accident severities</h4>
        <div>
            ${getCircle('yellow')}<span>Slight accident</span>
        </div>
        <div>
            ${getCircle('orange')}<span>Severe accident</span>
        </div>
        <div>
            ${getCircle('red')}<span>Fatal accident</span>
        </div>
      `;
      return div;
    };
    this.map.on('overlayadd', e => e.name === 'Accidents' ? this.accidentLegend.addTo(this.map) : null);
    this.map.on('overlayremove', e => e.name === 'Accidents' ? this.accidentLegend.remove() : null);
  }

  public initDashboardMap(lat: number, lon: number, zoom: number): void {
    if (this.miniMap) {
      this.miniMap.off();
      this.miniMap.remove();
    }
    this.miniMap = L.map('minimap').setView([lat, lon], zoom);
    this.miniMap.addLayer(new L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: 'Map data <a href="https://openstreetmap.org">OpenStreetMap</a> contributors',
      minZoom: 0,
      maxZoom: 19
    }));
  }

  public drawStationMarkers(): Promise<any> {
    return this.fetchBikePointGeoData().then((data) => {
      this.bikePoints = data;
      const markerClusters = L.markerClusterGroup({
        spiderflyOnMaxZoom: true,
        showCoverageOnHover: true,
        zoomToBoundsOnClick: true
      });
      this.mapOverlays.Bikepoints = markerClusters;
      this.map.addLayer(markerClusters);
      for (const station of data) {
        const marker = L.marker([station.lat, station.lon], {icon: createIcon('blue')});
        markerClusters.addLayer(marker);
        marker.on('click', e => {
          e.target.bindPopup(this.popUpService.makeAvailabilityPopUp(station), {maxWidth: 'auto'})
            .openPopup();
          this.map.panTo(e.target.getLatLng());
        });
        marker.on('popupclose', e => e.target.unbindPopup());
      }
    }).catch((error) => {
      console.log(error);
    });
  }

  public drawHeatmap(): void {
    const heatPoints = this.bikePoints.map(bikePoint => ([
      bikePoint.lat,
      bikePoint.lon,
      bikePoint.status.NbBikes
    ]));
    const heatmap = L.heatLayer(heatPoints, {
      max: 5,
      radius: 90
    });
    if (this.layerToDisplay.Heatmap) {
      this.map.addLayer(heatmap);
    }
    this.mapOverlays.Heatmap = heatmap;
  }

  public drawAccidents(): void {
    this.fetchAccidentGeoData().then(data => {
      const myRenderer = L.canvas({padding: 0.5});
      const accidents = [];
      for (const accident of data) {
        const severityColor = this.getAccidentColor(accident.severity);
        const accidentMarker = L.circle([accident.lat, accident.lon], {
          renderer: myRenderer,
          color: severityColor,
          fillColor: severityColor,
          fillOpacity: 0.5,
          radius: 30,
          interactive: false
        });
        accidents.push(accidentMarker);
      }
      const accidentLayer = L.layerGroup(accidents);
      if (this.layerToDisplay.Accidents) {
        this.map.addLayer(accidentLayer);
      }
      this.mapOverlays.Accidents = accidentLayer;
      this.drawMapControl();
    });
  }

  public getAccidentColor(severity: string): string {
    switch (severity) {
      case 'Slight':
        return 'yellow';
      case 'Serious':
        return 'orange';
      case 'Fatal':
        return 'red';
    }
  }

  public drawDashboardStationMarker(station: IDashboardCommonBikePoint): void {
    this.dashBoardBikePoint = station;
    this.dashBoardMarker = L.marker([station.lat, station.lon], {icon: createIcon('blue')}).addTo(this.miniMap);
    this.dashBoardMarker.on('mouseover', e => e.target.bindPopup(`<p>${station.commonName}</p>`).openPopup());
    this.dashBoardMarker.on('mouseout', e => e.target.closePopup());
  }

  public drawTableStationMarker(bikePoints: any[]): void {
    this.removeOverlayOnMiniMap();
    for (const point of bikePoints) {
      const marker = L.marker([point.stationLat, point.stationLon], {icon: createIcon(point.color)}).addTo(this.miniMap);
      marker.on('mouseover', e => e.target.bindPopup(`<p>${point.stationName}</p>`).openPopup());
      marker.on('mouseout', e => e.target.closePopup());
      this.drawLineOnMiniMap(marker, point);
      this.markerLayer.push(marker);
    }
    this.miniMap.fitBounds(L.featureGroup([this.dashBoardMarker, ...this.markerLayer]).getBounds());
    if (this.polylineLayer.length === 0) {
      this.legend.remove();
    } else {
      this.drawLegend();
    }
  }

  drawLegend(): void {
    this.legend.onAdd = () => {
      const div = L.DomUtil.create('div', 'legend');
      div.innerHTML += `<h4>Traffic lines</h4>`;
      div.innerHTML += `<i style="background: green"></i><span>inbound</span><br>`;
      div.innerHTML += `<i style="background: red"></i><span>outbound</span><br>`;
      div.innerHTML += `<i style="background: blue"></i><span>in- and outbound</span>`;
      return div;
    };
    this.legend.addTo(this.miniMap);
  }

  public removeOverlayOnMiniMap(): void {
    if (this.markerLayer) {
      this.markerLayer.forEach(marker => {
        this.miniMap.removeLayer(marker);
      });
      this.markerLayer = [];
      this.polylineLayer.forEach(polyline => {
        this.miniMap.removeLayer(polyline);
      });
      this.polylineLayer = [];
    }
    this.legend.remove();
  }

  private drawLineOnMiniMap(marker: L.marker, bikePoint: any): void {
    const latlngs = [];
    latlngs.push(this.dashBoardMarker.getLatLng());
    latlngs.push(marker.getLatLng());
    this.polylineLayer.push(L.polyline(latlngs, {color: bikePoint.polyLineColor}).addTo(this.miniMap));
  }

  private drawMapControl(): void {
    this.map.removeControl(this.layerControl);
    this.layerControl = L.control.layers(null, this.mapOverlays, {position: 'bottomright'});
    this.map.addControl(this.layerControl);
  }

  private fetchBikePointGeoData(): Promise<any> {
    return this.client.get(environment.apiUrl + 'latest/bikepoints/').toPromise();
  }

  private fetchAccidentGeoData(): Promise<any> {
    return this.client.get(environment.apiUrl + 'latest/accidents/2019').toPromise();
  }
}
