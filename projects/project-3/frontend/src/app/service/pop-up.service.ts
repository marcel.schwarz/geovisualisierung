import {ComponentFactoryResolver, Injectable, Injector} from '@angular/core';
import {IMapBikePoint} from './domain/map-bike-point';
import {PopUpComponent} from '../map/pop-up/pop-up.component';

@Injectable({
  providedIn: 'root'
})
export class PopUpService {

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    private injector: Injector
  ) {
  }

  makeAvailabilityPopUp(station: IMapBikePoint): any {
    const factory = this.componentFactoryResolver.resolveComponentFactory(PopUpComponent);
    const component = factory.create(this.injector);

    component.instance.station = station;
    component.changeDetectorRef.detectChanges();

    return component.location.nativeElement;
  }
}
