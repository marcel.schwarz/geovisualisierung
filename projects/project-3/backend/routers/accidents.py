import logging
from enum import Enum
from typing import List

from fastapi import APIRouter
from pydantic.main import BaseModel

import api_database

router = APIRouter(prefix="/accidents", tags=["accidents", "local"])
LOG = logging.getLogger()


class Severity(str, Enum):
    slight = "Slight"
    serious = "Serious"
    fatal = "Fatal"


class Accident(BaseModel):
    lat: float
    lon: float
    severity: Severity


@router.get(
    "/",
    name="Get all accidents",
    description="Get all bike accidents in London.",
    response_model=List[Accident]
)
def get_accidents():
    return api_database.get_all_accidents()


@router.get(
    "/{year}",
    name="Get accidents by year",
    description="Get bike accidents in London for a specific year.",
    response_model=List[Accident]
)
def get_accidents(year: str):
    return api_database.get_accidents(year)
