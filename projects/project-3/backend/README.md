# Backend Project

## 1. Install requirements
```shell
pip3 install -r requirements.txt
```

## 2. Init database
To init the database it is required that you have at least python 3.7 installed.

Generate the database with
```shell
python3 db_init.py
```

Note: Import can be interrupted to resume it later!  
Note: The importer outputs logs to the terminal, and the db_init.log file.

## 3. Start the backend
To start the backend, you first have to initialize the database!
### Run with docker
Switch into the project directory, then run:
```shell
docker build -t geovis-backend .
```
After the build make sure you are in the same directory as "bike-data.db" resides, if so, run
```shell
docker run -v $(pwd):/app -p 8080:80 --restart always -d geovis-backend
```

Note: `$(pwd)` puts the current directory in the command, if you are on Windows, you can use WSL or provide the full path by typing it out.

To stop just shut down the container.
