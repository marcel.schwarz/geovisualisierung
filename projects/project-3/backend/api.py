import uvicorn
from fastapi import FastAPI, APIRouter
from fastapi.middleware.cors import CORSMiddleware

from routers import accidents, bikepoints, dashboard

app = FastAPI(
    title="London Bikestations Dashboard API",
    docs_url="/api/docs",
    redoc_url="/api/redoc",
    openapi_url="/api/openapi.json"
)

origins = [
    "http://it-schwarz.net",
    "https://it-schwarz.net"
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

v1_router = APIRouter()
v1_router.include_router(accidents.router)
v1_router.include_router(bikepoints.router)
v1_router.include_router(dashboard.router)

app.include_router(v1_router, prefix="/api/latest")


if __name__ == "__main__":
    uvicorn.run("api:app", host="0.0.0.0", port=8080, reload=True)
