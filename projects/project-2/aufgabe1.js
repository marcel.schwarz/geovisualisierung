const aufgabe1 = () => {
    const map = L.map('map').setView([53.0, 14.0], 4)
    map.addLayer(new L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: 'Map data <a href="https://openstreetmap.org">OpenStreetMap</a> contributors',
        minZoom: 0,
        maxZoom: 18
    }))

    const levels = [0, 20, 50, 100, 200, 250, 300, 400]
    let geojson

    const getColor = density => {
        switch (true) {
            case density > levels[7]:
                return '#800026'
            case density > levels[6]:
                return '#BD0026'
            case density > levels[5]:
                return '#E31A1C'
            case density > levels[4]:
                return '#FC4E2A'
            case density > levels[3]:
                return '#FD8D3C'
            case density > levels[2]:
                return '#FEB24C'
            case density > levels[1]:
                return '#FED976'
            case density > levels[0]:
                return '#FFEDA0'
            default:
                return '#ffdea1'
        }
    }

    // Add the legend to the map
    const legend = L.control({position: 'bottomright'})

    legend.onAdd = () => {
        const legend = L.DomUtil.create('div', 'info legend')
        const labels = []

        for (let i = 0; i < levels.length; i++) {
            labels.push(
                '<i style="background:' + getColor(levels[i] + 1) + '"></i> '
                + levels[i] + (levels[i + 1] ? '&ndash;' + levels[i + 1] : '+')
            )
        }

        legend.innerHTML = labels.join('<br>')
        return legend
    }

    legend.addTo(map)

    // Control that shows state info on hover
    const info = L.control()

    info.onAdd = () => {
        info._div = L.DomUtil.create('div', 'info')
        info.update()
        return info._div
    }

    info.update = props => {
        info._div.innerHTML = '<h4>European Population Density</h4>'
        if (props) {
            let formattedDensity = new Intl.NumberFormat('de-DE').format(props.density)
            info._div.innerHTML += `<b>${props.name}</b><br>${formattedDensity} people/km<sup>2</sup>`
        } else {
            info._div.innerHTML += 'Hover over a country'
        }
    }

    info.addTo(map)

    // Hover over feature
    const onEachFeature = (feature, layer) => {
        layer.on({
            mouseover: event => {
                event.target.setStyle({
                    weight: 5,
                    color: '#666',
                    dashArray: '',
                    fillOpacity: 0.7
                })

                if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
                    event.target.bringToFront()
                }
                info.update(event.target.feature.properties)
            },
            mouseout: event => {
                geojson.resetStyle(event.target)
                info.update()
            },
            click: event => map.fitBounds(event.target.getBounds())
        })
    }

    // Default style for each feature
    const getStyle = feature => ({
        stroke: true,
        fill: true,
        fillColor: getColor(feature.properties.density),
        fillOpacity: 1,
        dashArray: '3',
        weight: 2,
        color: 'black'
    })

    // Load data in
    $.getJSON('europe.geo.json', data => {
        data.features.map(feature => feature.properties.density = feature.properties.pop_est / (turf.area(feature) / 1_000_000))

        geojson = L.geoJson(data, {style: getStyle, onEachFeature}).addTo(map)
    })
}

aufgabe1()
