const aufgabe2 = () => {

    const createIcon = imgName => L.icon({
        iconUrl: `markers/${imgName}.png`,
        iconSize: [60, 60],
        iconAnchor: [30, 60],
        popupAnchor: [0, -53]
    })

    const markerIcons = {
        food: createIcon("food"),
        culture: createIcon("culture"),
        nature: createIcon("landscape"),
        train: createIcon("train")
    }

    let map = L.map('poiMap').setView([48.779694, 9.177015], 14);
    map.addLayer(new L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: 'Map data <a href="https://openstreetmap.org">OpenStreetMap</a> contributors',
        minZoom: 0,
        maxZoom: 19
    }))

    const createFoodMarker = (coords, text) => L.marker(coords, {icon: markerIcons.food}).bindPopup(`<b>Essen und Trinken</b><br>${text}`)
    const createCultureMarker = (coords, text) => L.marker(coords, {icon: markerIcons.culture}).bindPopup(`<b>Kunst und Kultur</b><br>${text}`)
    const createNatureMarker = (coords, text) => L.marker(coords, {icon: markerIcons.nature}).bindPopup(`<b>Natur</b><br>${text}`)
    const createTrainMarker = (coords, text) => L.marker(coords, {icon: markerIcons.train}).bindPopup(`<b>&Ouml;PNV</b><br>${text}`)

    const foodLayer = L.layerGroup([
        createFoodMarker([48.780980, 9.169636], "Mensa Stuttgart"),
        createFoodMarker([48.778051, 9.176946], "Ochs'n Willi"),
        createFoodMarker([48.779674, 9.178160], "K&ouml;nigsbau Passagen"),
        createFoodMarker([48.780378, 9.172597], "HFT Block 4")
    ])

    const cultureLayer = L.layerGroup([
        createCultureMarker([48.788165, 9.233864], "Mercedes-Benz Museum"),
        createCultureMarker([48.793296, 9.228010], "Porsche Arena"),
        createCultureMarker([48.834270, 9.152479], "Porsche Museum"),
        createCultureMarker([48.755866, 9.190109], "Fernsehturm"),
        createCultureMarker([48.780149, 9.186586], "Staatsgalerie"),
        createCultureMarker([48.786550, 9.084028], "Schloss Solitude")
    ])

    const natureLayer = L.layerGroup([
        createNatureMarker([48.804148, 9.208100], "Wilhelma"),
        createNatureMarker([48.782037, 9.268643], "Grabkapelle auf dem W&uuml;rttemberg"),
        createNatureMarker([48.785969, 9.187023], "Schlossgarten"),
        createNatureMarker([48.818362, 9.184177], "Aussichtspunkt Burgholzhof")
    ])

    const trainLayer = L.layerGroup([
        createTrainMarker([48.777440, 9.173764], "Roteb&uuml;hlplatz"),
        createTrainMarker([48.784760, 9.182898], "Stuttgart Hauptbahnhof"),
        createTrainMarker([48.780173, 9.177220], "B&ouml;rsenplatz"),
        createTrainMarker([48.755939, 9.142164], "Seilbahn"),
        createTrainMarker([48.764290, 9.168611], "Zahnradbahn"),
    ])

    const overlayMaps = {
        'Essen und Trinken': foodLayer,
        'Kunst und Kultur': cultureLayer,
        'Natur': natureLayer,
        '&Ouml;PNV': trainLayer
    }

    Object.values(overlayMaps).forEach(layer => map.addLayer(layer))
    L.control.layers(null, overlayMaps).addTo(map)
}

aufgabe2()
